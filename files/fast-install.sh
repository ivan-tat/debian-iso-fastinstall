#!/bin/sh

# Install eatmydata-udeb for non-base system install on classic media and package uninstallation step on live media.
anna-install eatmydata-udeb

# On live images, final package removal process takes a lot of time without eatmydata,
# but it becomes disabled earlier than this step is performed. Fix this.
if [[ "$(cat /cdrom/.disk/cd_type)" == "live" ]]; then
  cnt=0
  # Wait up to 10 seconds until eatmydata-udeb is installed
  while [ ! -f "/usr/lib/finish-install.d/13eatmydata-udeb" ]; do
    sleep 1
    cnt=$(($cnt+1))
    [[ "$cnt" == "10" ]] && break
  done
  if [ -f "/usr/lib/finish-install.d/13eatmydata-udeb" ]; then
    # Move eatmydata disabling after 60remove-live-packages
    mv /usr/lib/finish-install.d/13eatmydata-udeb /usr/lib/finish-install.d/63eatmydata-udeb
  fi
else
# On classic images, debootstrap needs to be patched to install eatmydata into target OS
# and preload the library in chroot installation stage.
  for patchfile in /usr/share/debootstrap/scripts/*; do
  # Patch debootstrap
  # See https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=700633
  sed -i 's~required="$(get_debs Priority: required)"~required="$(get_debs Priority: required) eatmydata"~g' "$patchfile"
  sed -i 's~second_stage_install () {~second_stage_install () { export LD_PRELOAD=libeatmydata.so;~' "$patchfile"
  done
fi
